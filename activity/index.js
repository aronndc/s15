console.log(
    "Part 1 Activity Start"
 )
 console.log("");

let myFirstName = "Louies Aronn Dela Cerna";
let myLastName ="Pineda";
let myAge = 29;
let myHobbies = [" Reading books "," Appreciating nature "," Taking photos "];


let workAddress = {
    houseNumber: "33F Rufino Pacific Tower",
    street: "Ayala Ave.,",
    city: "Makati City",
    state: "Naional Capital Region"
};

console.log("My First Name: "+myFirstName);
console.log("My Last Name: "+myLastName);
console.log("My Age is: "+ myAge + " yrs old");
console.log("My hobbies listed in array: " + myHobbies);
console.log("My Work Address is listed below in an Object:");
console.log(workAddress);
console.log("");

console.log("Part 1 Activity End")


/*
    2. Debugging Practice - Identify and implement the best practices of creating and using variables 
       by avoiding errors and debugging the following codes:

            -Log the values of each variable to follow/mimic the output.
*/
console.log("");

console.log(
    "Part 2 Activity Start"
 )
 console.log("");

    let fullName = "Steve Rogers";
    console.log("My full name is " + fullName);

    let currentAge = 40;
    console.log("My current age is: " + currentAge);

    let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
    console.log("My Friends are: " + friends)


    let profile = {

        username: "Captain America",
        fullName2: "Steve Rogers",
        age: 40,
        isActive: false,

    }
    console.log("My Full Profile: ")
    console.log(profile);

    let fullName2 = "Bucky Barnes";
    console.log("My bestfriend is: " + fullName2);

    let lastLocation = "Arctic Ocean";
    lastLocation = "Atlantic Ocean";
    console.log("I was found frozen in: " + lastLocation);

    console.log("");

    console.log("Part 2 Activity End")

    console.log("");